"""Homework 7 Task 3 implementation."""

import math
from typing import Generator


def prime_numbers(n: int) -> Generator:
    """Return first N prime numbers."""

    def is_prime(num: int) -> bool:
        """Check whether number is prime."""
        if num < 2:
            return False
        for idx in range(2, int(math.sqrt(num)) + 1):
            if num % idx == 0:
                return False
        return True

    counter = 0
    number = 2
    while counter < n:
        if is_prime(number):
            yield number
            counter += 1
        number += 1


if __name__ == '__main__':

    gen1 = prime_numbers(20)
    print(type(gen1))
    for item in gen1:
        print(item, end=' ')
