"""Homework 7 Task 1 implementation."""

from typing import Generator


def reversed_generator(items: list) -> Generator:
    """Generator that returns elements reversed."""

    for idx in range(len(items) - 1, -1, -1):
        yield items[idx]


if __name__ == '__main__':

    l1 = list(range(5))
    print(l1)

    gen1 = reversed_generator(l1)
    print(type(gen1))

    for item in gen1:
        print(item)
