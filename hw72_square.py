"""Homework 7 Task 2 implementation."""


l1 = list(range(1, 11))
print(l1)
print()

sq_evens = (item ** 2 for item in l1 if item % 2 == 0)
print(type(sq_evens))
for sq_even in sq_evens:
    print(sq_even)
print()

for element in l1:
    if element % 2 == 0:
        print(element ** 2)
